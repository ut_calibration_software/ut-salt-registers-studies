import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle
import json
import re
import yaml
import math



###################################

def load_UT_dict(INPUT_PATH):
    vector_like_registers = ['pedestals_cfg', 'baselines_cfg', 'masks_cfg', 'calib_enables_cfg']
    
    INPUT_FMT = INPUT_PATH.split(".")[-1]

    UT_dict = {
            'UADAQFEE': {},
            'UCDAQFEE': {}
            }

    if (INPUT_FMT == 'txt'):
        
        f = open(INPUT_PATH, 'r')
        lines = f.read().splitlines()

        i = 0
        nlines = len(lines)
        while i < nlines:
            UT_side, UT_sector, UT_chip, UT_register, UT_value = re.split(':|\.|,| ', lines[i])
            
            UT_dict[UT_side][UT_sector] = {}
            UT_dict[UT_side][UT_sector][UT_chip] = {}
            UT_dict[UT_side][UT_sector][UT_chip][UT_register] = UT_value
            
            
                
            for new_sector_index in range(i+1, nlines):                             #Find the line where the next UT_sector begins
                new_UT_sector = re.split(':|\.|,| ', lines[new_sector_index])[1]
                if new_UT_sector != UT_sector: break
            
            if new_sector_index == nlines - 1:
                new_sector_index += 1
                
            
            reg_index = i + 1                                                       #Move to the next line to find the next register

            while reg_index < new_sector_index:                                     #Check if we haven't moved to the next sector
            
                for new_chip_index in range(reg_index, new_sector_index):           #Find the line where the next UT_chip begins
                    new_UT_chip = re.split(':|\.|,| ', lines[new_chip_index])[2]    #Create empty dicts for each chip number
                    if new_UT_chip != UT_chip:
                        UT_dict[UT_side][UT_sector][new_UT_chip] = {}
                        break
            
                if(new_chip_index == (new_sector_index - 1)): new_chip_index += 1
                
                    
                
       
                while reg_index < new_chip_index:                                  #Fill out another registers for the UT_chip

                    UT_chip, UT_register, UT_value = re.split(':|\.|,| ', lines[reg_index])[2:]   #Added UT_chip to make it work for a file with only one register per chip
                    
                    if UT_register in ['pedestals_cfg', 'baselines_cfg']:
                        UT_dict[UT_side][UT_sector][UT_chip][UT_register] = [UT_value[i:i+2] for i in range(0, len(UT_value), 2)] 
                    elif UT_register in ['masks_cfg', 'calib_enables_cfg']:
                        masks_tab = [UT_value[i:i+2] for i in range(0, len(UT_value), 2)]
                        masks_str = ""
                        for val in masks_tab:
                            masks_str += f"{int(val, 16):08b}"[::-1]
                        UT_dict[UT_side][UT_sector][UT_chip][UT_register] = [int(masks_str[i]) for i in range(len(masks_str))]
                    else:
                        UT_dict[UT_side][UT_sector][UT_chip][UT_register] = UT_value
                    reg_index += 1
                
                UT_chip = new_UT_chip
                


                
    
            i = reg_index
            
        if(len(UT_dict['UADAQFEE'])) == 0: del UT_dict['UADAQFEE']
        if(len(UT_dict['UCDAQFEE'])) == 0: del UT_dict['UCDAQFEE']
        
        f.close()
            
    elif(INPUT_FMT == 'json'):
        f = open(INPUT_PATH)
        UT_dict = json.load(f)
        f.close()
        
    return UT_dict

###########################################

def get_bits(value, start_bit=None, end_bit=None, twos_complement=False):
    # If start_bit and end_bit are not specified, return the original value
    if start_bit is None or end_bit is None:
        return value

    # Create a mask that has 1s in the positions of the desired bits
    mask = (1 << (end_bit - start_bit + 1)) - 1
    # Shift the value to the right to align the desired bits with the least significant bit, then apply the mask
    value = (value >> start_bit) & mask
    
    
    # Decipher from two's complement of the value is negative 
    if twos_complement and (value & (1 << (end_bit - start_bit))):
        value -= 1 << (end_bit - start_bit + 1)

    return value

############################################

def draw_UT(tab, plane, fig, ax, side, **kwargs):
    im = ax.imshow(tab, origin="lower", extent=(0,36,0,28), aspect = 'auto', **kwargs)

    # ax = plt.gca()
    ax.set_xticks(np.arange(0, 36, 2))
    ax.set_yticks(np.arange(0, 28, 2))
    ax.set_xticklabels([])
    ax.set_yticklabels([])

    ys = ["M4B", "S3B", "M3B", "S2B", "M2B", "S1B", "M1B", "M1T", "S1T", "M2T", "S2T", "M3T", "S3T", "M4T"]
    staves = ['S9', 'S8', 'S7', 'S6', 'S5', 'S4', 'S3', 'S2', 'S1', 'S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8', 'S9']

    ax.set_xticks(np.arange(1, 37, 2), minor=True)
    ax.set_xticklabels(staves, minor=True)
    ax.set_yticks(np.arange(1, 29, 2), minor=True)
    ax.set_yticklabels(ys, minor=True)

    # Gridlines based on minor ticks
    ax.grid(color='black', linestyle=':', linewidth=1)

    ax.plot([15,15], [10,18], ls = ':', color = 'black', lw=1)
    ax.plot([17,17], [10,18], ls = ':', color = 'black', lw=1)
    ax.plot([19,19], [10,18], ls = ':', color = 'black', lw=1)
    ax.plot([21,21], [10,18], ls = ':', color = 'black', lw=1)
    ax.plot([16,20], [13,13], ls = ':', color = 'black', lw=1)
    ax.plot([16,20], [15,15], ls = ':', color = 'black', lw=1)

    ax.plot([14,14], [10,18], ls = '-', color = 'black', lw=2)
    ax.plot([22,22], [10,18], ls = '-', color = 'black', lw=2)
    ax.plot([16,16], [12,16], ls = '-', color = 'black', lw=2)
    ax.plot([20,20], [12,16], ls = '-', color = 'black', lw=2)
    ax.plot([14,22], [10,10], ls = '-', color = 'black', lw=2)
    ax.plot([14,22], [18,18], ls = '-', color = 'black', lw=2)
    ax.plot([16,20], [12,12], ls = '-', color = 'black', lw=2)
    ax.plot([16,20], [16,16], ls = '-', color = 'black', lw=2)



    ax2 = ax.twiny()
    ax2.set_xticks(np.arange(0, 37, 18))
    ax2.set_xticklabels([])
    ax2.grid(color='black', linestyle='-', linewidth=3)
    ax2.tick_params(axis='x', colors='white') 

    ax3 = ax.twinx()
    ax3.set_yticks(np.arange(0, 29, 14))
    ax3.set_yticklabels([])
    ax3.grid(color='black', linestyle='-', linewidth=3)
    ax3.tick_params(axis='y', colors='white') 

    ax3.plot(18,14, 'wo', ms = 10, mec = 'gray', mew = '1')

    cbar = plt.colorbar(im, ax = ax)
    cbar.ax.set_yscale('linear')


    ax.set_title(plane)
    fig.patch.set_facecolor('white')
    
    x_min = 0
    x_max = 36


    if (plane == "UTaX") or (plane == "UTaU"):
        x_min = 2
        x_max = 34
        
    if plane not in ["UTaX", "UTaU", "UTbV", "UTbX"]:
        raise ValueError("Wrong UT plane name!")
    
    if side == "A":
        x_max = 18
    elif side == "C":
        x_min = 18
    elif (side != None):
        raise ValueError("Wrong UT side name!")
        

    
    ax.set_xlim([x_min, x_max])
    ax2.set_xlim([x_min, x_max])
    ax3.set_xlim([x_min, x_max])
    
    return [fig, ax, im, cbar]

#################################################

class UT_evolution:
    def __init__(self, recipes):


        with open("./bi_translator.pkl", 'rb') as handle:
            self.bi_translator = pickle.load(handle) 
            
        with open("./universal_map.pkl", 'rb') as handle:
            self.universal_map = pickle.load(handle)  
            
        with open("./UT_map.pkl", 'rb') as handle:
            self.UT_map = pickle.load(handle)
            
        with open("./UT_mirrored_map.pkl", 'rb') as handle:
            self.UT_mirrored_map = pickle.load(handle) 
        
        self.UT_sides = ["UADAQFEE", "UCDAQFEE"]
        self.UT_planes = ["UTaX", "UTaU", "UTbV", "UTbX"]
        self.UT_sensor_types = ["A-type", "B-type", "C-type", "D-type"]
        self.vector_regs = ["masks_cfg", "pedestals_cfg", "baselines_cfg", "calib_enables_cfg"]
        
        recipes.sort()
        self.recipes = recipes
        self.nrecipes = len(recipes)
        self.dates = []
        self.date_to_index = {}

        for i in range(self.nrecipes):
            time_str = self.recipes[i].split('_')[-1].split('.')[0]
            date = time_str.split('T')[0]
            date = date[:4] + '/' + date[4:6] + "/" + date[6:] 
            hours = time_str.split('T')[1:]
            hours = [hour[:2] + ":" + hour[2:4] + ":" + hour[4:] for hour in hours]
            timestamp = date + " "
            for hour in hours:
                timestamp += (hour + ", ")
            timestamp = timestamp[:-2]
            self.dates.append(timestamp)
            self.date_to_index[timestamp] = i    
        
        
        self.data = np.empty(self.nrecipes, dtype=dict)
        for i in range(self.nrecipes):
            self.data[i] = load_UT_dict(self.recipes[i])
    
    def at(self, index):
        return self.data[index]
    
    def at_date(self, date):
        return self.data[self.date_to_index[date]]
    
    def _check_filters(self, ECS, cond_dict):
        if len(cond_dict) == 0:
            return True
        else:
            return (np.all([np.any([self.universal_map[ECS][key] == value[i] for i in range(len(value))]) for key, value in cond_dict.items()]))
    
    def get_data(self, UT_region, register, dates = None, filters = "", start_bit = None, end_bit = None, twos_complement=False, exclude_masked = True):
        
        if dates == None:
            dates = self.dates
            
        if np.shape(dates) == ():
            dates = [dates]
            
        cond_dict = {}
            
        if filters != "":
            for cond in filters.split(","):
                cond_tab = cond.split("==")
                key = cond_tab[0].replace(" ", "")
                val = cond_tab[1].replace(" ", "")
                
                val = val.split("|")
                cond_dict[key] = val
        
        data = []
        masks = []
        for t, date in enumerate(dates):
            
            UT_dict = self.at_date(date)

            
            ### Whole UT
        
            if UT_region == 'UT':
                for UT_side in UT_dict.keys():
                    for UT_sector in UT_dict[UT_side].keys():
                        for UT_chip in UT_dict[UT_side][UT_sector].keys():
                            ECS = UT_sector + "." + UT_chip
                            if(self._check_filters(ECS, cond_dict)):
                                UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                if np.shape(UT_value) == ():
                                    if exclude_masked:
                                        if (UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"] != (128*[1])):
                                            data.append(get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement))
                                        else:
                                            data.append(np.nan)
                                    else:
                                        data.append(get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement))
                                elif register in ["pedestals_cfg", "baselines_cfg"]:
                                    if exclude_masked:
                                        masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]      
                                        data.append([get_bits(int(UT_value[i], 16), start_bit, end_bit, twos_complement) if masks[i] == 0 else np.nan for i in range(len(UT_value))])
                                    else:
                                        data.append([get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value])    
                                else:
                                    if exclude_masked:
                                        masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]      
                                        data.append([UT_value[i] if masks[i] == 0 else np.nan for i in range(len(UT_value))])
                                    else:
                                        data.append(UT_value)         
            ### UT-sides:
            elif UT_region in self.UT_sides:
                if UT_region in UT_dict.keys():
                    UT_side = UT_region
                    for UT_sector in UT_dict[UT_side].keys():
                        for UT_chip in UT_dict[UT_side][UT_sector].keys():
                            ECS = UT_sector + "." + UT_chip
                            if(self._check_filters(ECS, cond_dict)):
                                UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                if np.shape(UT_value) == ():
                                    if exclude_masked:
                                        if (UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"] != (128*[1])):
                                            data.append(get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement))
                                        else:
                                            data.append(np.nan)
                                    else:
                                        data.append(get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement))
                                elif register in ["pedestals_cfg", "baselines_cfg"]:
                                    if exclude_masked:
                                        masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]      
                                        data.append([get_bits(int(UT_value[i], 16), start_bit, end_bit, twos_complement) if masks[i] == 0 else np.nan for i in range(len(UT_value))])
                                    else:
                                        data.append([get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value])    
                                else:
                                    if exclude_masked:
                                        masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]      
                                        data.append([UT_value[i] if masks[i] == 0 else np.nan for i in range(len(UT_value))])
                                    else:
                                        data.append(UT_value)                               
                else:
                    raise ValueError("Data for this UT-side not present in the recipe!")
                
            ### UT-planes:
            elif UT_region in self.UT_planes:
                for UT_side in UT_dict.keys():
                    for UT_sector in UT_dict[UT_side].keys():
                        if UT_region in UT_sector:
                            for UT_chip in UT_dict[UT_side][UT_sector].keys():
                                ECS = UT_sector + "." + UT_chip
                                if(self._check_filters(ECS, cond_dict)):
                                    UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                    if np.shape(UT_value) == ():
                                        if exclude_masked:
                                            if (UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"] != (128*[1])):
                                                data.append(get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement))
                                            else:
                                                data.append(np.nan)
                                        else:
                                            data.append(get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement))
                                    elif register in ["pedestals_cfg", "baselines_cfg"]:
                                        if exclude_masked:
                                            masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]      
                                            data.append([get_bits(int(UT_value[i], 16), start_bit, end_bit, twos_complement) if masks[i] == 0 else np.nan for i in range(len(UT_value))])
                                        else:
                                            data.append([get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value])    
                                    else:
                                        if exclude_masked:
                                            masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]      
                                            data.append([UT_value[i] if masks[i] == 0 else np.nan for i in range(len(UT_value))])
                                        else:
                                            data.append(UT_value)                                                       
                                      
                                    
            ### UT-sides + UT-planes:
            elif np.any([(side in UT_region) for side in UT_dict.keys()]) and np.any([(plane in UT_region) for plane in self.UT_planes]):
                UT_side, UT_plane = UT_region.split(".")
                for UT_sector in UT_dict[UT_side].keys():
                    if UT_plane in UT_sector:
                        for UT_chip in UT_dict[UT_side][UT_sector].keys():
                            ECS = UT_sector + "." + UT_chip
                            if(self._check_filters(ECS, cond_dict)):
                                UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                if np.shape(UT_value) == ():
                                    if exclude_masked:
                                        if (UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"] != (128*[1])):
                                            data.append(get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement))
                                        else:
                                            data.append(np.nan)
                                    else:
                                        data.append(get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement))
                                elif register in ["pedestals_cfg", "baselines_cfg"]:
                                    if exclude_masked:
                                        masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]      
                                        data.append([get_bits(int(UT_value[i], 16), start_bit, end_bit, twos_complement) if masks[i] == 0 else np.nan for i in range(len(UT_value))])
                                    else:
                                        data.append([get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value])    
                                else:
                                    if exclude_masked:
                                        masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]      
                                        data.append([UT_value[i] if masks[i] == 0 else np.nan for i in range(len(UT_value))])
                                    else:
                                        data.append(UT_value)                       
                
                                    
            ### UT-staves:
            elif np.any([(plane in UT_region) for plane in self.UT_planes]) and '_' in UT_region and len(UT_region) == 7:
                UT_plane, UT_stave = UT_region.split("_")
                if 'A' in UT_stave: UT_side = 'UADAQFEE'
                elif 'C' in UT_stave: UT_side  = 'UCDAQFEE'
                for UT_sector in UT_dict[UT_side].keys():
                    if UT_stave in UT_sector and UT_plane in UT_sector:
                        for UT_chip in UT_dict[UT_side][UT_sector].keys():
                            ECS = UT_sector + "." + UT_chip
                            if(self._check_filters(ECS, cond_dict)):
                                UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                if np.shape(UT_value) == ():
                                    if exclude_masked:
                                        if (UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"] != (128*[1])):
                                            data.append(get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement))
                                        else:
                                            data.append(np.nan)
                                    else:
                                        data.append(get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement))
                                elif register in ["pedestals_cfg", "baselines_cfg"]:
                                    if exclude_masked:
                                        masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]      
                                        data.append([get_bits(int(UT_value[i], 16), start_bit, end_bit, twos_complement) if masks[i] == 0 else np.nan for i in range(len(UT_value))])
                                    else:
                                        data.append([get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value])    
                                else:
                                    if exclude_masked:
                                        masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]      
                                        data.append([UT_value[i] if masks[i] == 0 else np.nan for i in range(len(UT_value))])
                                    else:
                                        data.append(UT_value)                  
                            
                                    
            ### UT-sectors:          
            else:
                if np.any([UT_region in UT_dict[UT_side].keys() for UT_side in UT_dict.keys()]):
                    UT_sector = UT_region
                    if 'A' in UT_sector.split("_")[1]: UT_side = 'UADAQFEE'
                    if 'C' in UT_sector.split("_")[1]: UT_side = 'UCDAQFEE' 
                    for UT_chip in UT_dict[UT_side][UT_sector].keys():
                        ECS = UT_sector + "." + UT_chip
                        if(self._check_filters(ECS, cond_dict)):
                            UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                            if np.shape(UT_value) == ():
                                if exclude_masked:
                                    if (UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"] != (128*[1])):
                                        data.append(get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement))
                                    else:
                                        data.append(np.nan)
                                else:
                                    data.append(get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement))
                            elif register in ["pedestals_cfg", "baselines_cfg"]:
                                if exclude_masked:
                                    masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]      
                                    data.append([get_bits(int(UT_value[i], 16), start_bit, end_bit, twos_complement) if masks[i] == 0 else np.nan for i in range(len(UT_value))])
                                else:
                                    data.append([get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value])    
                            else:
                                if exclude_masked:
                                    masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]      
                                    data.append([UT_value[i] if masks[i] == 0 else np.nan for i in range(len(UT_value))])
                                else:
                                    data.append(UT_value)                            
                                
            
            ### UT-chips
            
                elif len(UT_region.split(".")) == 2:                    
                    UT_sector, UT_chip = UT_region.split(".")
                    if 'A' in UT_sector.split("_")[1]: UT_side = 'UADAQFEE'
                    if 'C' in UT_sector.split("_")[1]: UT_side = 'UCDAQFEE' 
                    
                    if UT_sector in UT_dict[UT_side].keys():
                        if UT_chip in UT_dict[UT_side][UT_sector].keys():
                            ECS = UT_sector + "." + UT_chip
                            if(self._check_filters(ECS, cond_dict)):
                                UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                if np.shape(UT_value) == ():
                                    if exclude_masked:
                                        if (UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"] != (128*[1])):
                                            data.append(get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement))
                                        else:
                                            data.append(np.nan)
                                    else:
                                        data.append(get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement))
                                elif register in ["pedestals_cfg", "baselines_cfg"]:
                                    if exclude_masked:
                                        masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]      
                                        data.append([get_bits(int(UT_value[i], 16), start_bit, end_bit, twos_complement) if masks[i] == 0 else np.nan for i in range(len(UT_value))])
                                    else:
                                        data.append([get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value])    
                                else:
                                    if exclude_masked:
                                        masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]      
                                        data.append([UT_value[i] if masks[i] == 0 else np.nan for i in range(len(UT_value))])
                                    else:
                                        data.append(UT_value)                                   
                            
                        else:
                            raise ValueError(f"Not recognised UT region: {UT_region}!")
                    else:
                        raise ValueError(f"Not recognised UT region {UT_region}!")
                else:
                    raise ValueError(f"Not recognised UT region {UT_region}!")
                       
        data = np.array(data).flatten()
        data = data[~np.isnan(data)]                         
        return data
    
    def get_data_location(self, UT_region, register, dates = None, filters = "", start_bit = None, end_bit = None, twos_complement=False, exclude_masked = True):
        if dates == None:
            dates = self.dates
            
        if np.shape(dates) == ():
            dates = [dates]
            
        cond_dict = {}
            
        if filters != "":
            for cond in filters.split(","):
                cond_tab = cond.split("==")
                key = cond_tab[0].replace(" ", "")
                val = cond_tab[1].replace(" ", "")
                
                val = val.split("|")
                cond_dict[key] = val
        
        data = {}
        for t, date in enumerate(dates):
            
            data[date] = {}
            
            UT_dict = self.at_date(date)
            
            ### Whole UT
        
            if UT_region == 'UT':
                for UT_side in UT_dict.keys():
                    for UT_sector in UT_dict[UT_side].keys():
                        for UT_chip in UT_dict[UT_side][UT_sector].keys():
                            ECS = UT_sector + "." + UT_chip
                            if(self._check_filters(ECS, cond_dict)):
                                UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                if np.shape(UT_value) == ():
                                    data[date][UT_sector + "." + UT_chip] = get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement)
                                elif register in ["pedestals_cfg", "baselines_cfg"]:
                                    data[date][UT_sector + "." + UT_chip] = [get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value]              
                                else:
                                    data[date][UT_sector + "." + UT_chip] = UT_value
            
            ### UT-sides:
            elif UT_region in self.UT_sides:
                if UT_region in UT_dict.keys():
                    UT_side = UT_region
                    for UT_sector in UT_dict[UT_side].keys():
                        for UT_chip in UT_dict[UT_side][UT_sector].keys():
                            ECS = UT_sector + "." + UT_chip
                            if(self._check_filters(ECS, cond_dict)):
                                UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                if np.shape(UT_value) == ():
                                    data[date][UT_sector + "." + UT_chip] = get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement)
                                elif register in ["pedestals_cfg", "baselines_cfg"]:
                                    data[date][UT_sector + "." + UT_chip] = [get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value]              
                                else:
                                    data[date][UT_sector + "." + UT_chip] = UT_value                               
                else:
                    raise ValueError("Data for this UT-side not present in the recipe!")
                
            ### UT-planes:
            elif UT_region in self.UT_planes:
                for UT_side in UT_dict.keys():
                    for UT_sector in UT_dict[UT_side].keys():
                        if UT_region in UT_sector:
                            for UT_chip in UT_dict[UT_side][UT_sector].keys():
                                ECS = UT_sector + "." + UT_chip
                                if(self._check_filters(ECS, cond_dict)):
                                    UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                    if np.shape(UT_value) == ():
                                        data[date][UT_sector + "." + UT_chip] = get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement)
                                    elif register in ["pedestals_cfg", "baselines_cfg"]:
                                        data[date][UT_sector + "." + UT_chip] = [get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value]              
                                    else:
                                        data[date][UT_sector + "." + UT_chip] = UT_value                                                       
                                      
                                    
            ### UT-sides + UT-planes:
            elif np.any([(side in UT_region) for side in UT_dict.keys()]) and np.any([(plane in UT_region) for plane in self.UT_planes]):
                UT_side, UT_plane = UT_region.split(".")
                for UT_sector in UT_dict[UT_side].keys():
                    if UT_plane in UT_sector:
                        for UT_chip in UT_dict[UT_side][UT_sector].keys():
                            ECS = UT_sector + "." + UT_chip
                            if(self._check_filters(ECS, cond_dict)):
                                UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                if np.shape(UT_value) == ():
                                    data[date][UT_sector + "." + UT_chip] = get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement)
                                elif register in ["pedestals_cfg", "baselines_cfg"]:
                                    data[date][UT_sector + "." + UT_chip] = [get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value]              
                                else:
                                    data[date][UT_sector + "." + UT_chip] = UT_value                      
                
                                    
            ### UT-staves:
            elif np.any([(plane in UT_region) for plane in self.UT_planes]) and '_' in UT_region and len(UT_region) == 7:
                UT_plane, UT_stave = UT_region.split("_")
                if 'A' in UT_stave: UT_side = 'UADAQFEE'
                elif 'C' in UT_stave: UT_side  = 'UCDAQFEE'
                for UT_sector in UT_dict[UT_side].keys():
                    if UT_stave in UT_sector and UT_plane in UT_sector:
                        for UT_chip in UT_dict[UT_side][UT_sector].keys():
                            ECS = UT_sector + "." + UT_chip
                            if(self._check_filters(ECS, cond_dict)):
                                UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                if np.shape(UT_value) == ():
                                    data[date][UT_sector + "." + UT_chip] = get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement)
                                elif register in ["pedestals_cfg", "baselines_cfg"]:
                                    data[date][UT_sector + "." + UT_chip] = [get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value]              
                                else:
                                    data[date][UT_sector + "." + UT_chip] = UT_value                     
                            
                                    
            ### UT-sectors:          
            else:
                if np.any([UT_region in UT_dict[UT_side].keys() for UT_side in UT_dict.keys()]):
                    UT_sector = UT_region
                    if 'A' in UT_sector.split("_")[1]: UT_side = 'UADAQFEE'
                    if 'C' in UT_sector.split("_")[1]: UT_side = 'UCDAQFEE' 
                    for UT_chip in UT_dict[UT_side][UT_sector].keys():
                        ECS = UT_sector + "." + UT_chip
                        if(self._check_filters(ECS, cond_dict)):
                            UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                            if np.shape(UT_value) == ():
                                data[date][UT_sector + "." + UT_chip] = get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement)
                            elif register in ["pedestals_cfg", "baselines_cfg"]:
                                data[date][UT_sector + "." + UT_chip] = [get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value]              
                            else:
                                data[date][UT_sector + "." + UT_chip] = UT_value                                       
                                
            
            ### UT-chips
            
                elif len(UT_region.split(".")) == 2:                    
                    UT_sector, UT_chip = UT_region.split(".")
                    if 'A' in UT_sector.split("_")[1]: UT_side = 'UADAQFEE'
                    if 'C' in UT_sector.split("_")[1]: UT_side = 'UCDAQFEE' 
                    
                    if UT_sector in UT_dict[UT_side].keys():
                        if UT_chip in UT_dict[UT_side][UT_sector].keys():
                            ECS = UT_sector + "." + UT_chip
                            if(self._check_filters(ECS, cond_dict)):
                                UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                if np.shape(UT_value) == ():
                                    data[date][UT_sector + "." + UT_chip] = get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement)
                                elif register in ["pedestals_cfg", "baselines_cfg"]:
                                    data[date][UT_sector + "." + UT_chip] = [get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value]              
                                else:
                                    data[date][UT_sector + "." + UT_chip] = UT_value                                 
                            
                        else:
                            raise ValueError(f"Not recognised UT region {UT_region}!")
                    else:
                        raise ValueError(f"Not recognised UT region {UT_region}!")
                else:
                    raise ValueError(f"Not recognised UT region {UT_region}!")           
        return data
    
    def get_mean(self, UT_region, register, dates = None, filters = "", start_bit = None, end_bit = None, twos_complement=False, exclude_masked = True):
        
        data = self.get_data(UT_region, register, dates, filters, start_bit, end_bit, twos_complement, exclude_masked)
        
        return np.mean(data)
    
    def get_std(self, UT_region, register, dates = None, filters = "", start_bit = None, end_bit = None, twos_complement=False,  exclude_masked = True):
        
        data = self.get_data(UT_region, register, dates, filters, start_bit, end_bit, twos_complement, exclude_masked)
        
        return np.std(data)
    
    def get_rms(self, UT_region, register, dates = None, filters = "", start_bit = None, end_bit = None, twos_complement=False, exclude_masked = True):
        
        data = self.get_data(UT_region, register, dates, filters, start_bit, end_bit, twos_complement, exclude_masked)
        data = np.array(data)
        
        return np.sqrt(np.mean(data**2))
    
    def get_min(self, UT_region, register, dates = None, filters = "", start_bit = None, end_bit = None, twos_complement=False, exclude_masked = True):
        
        data = self.get_data(UT_region, register, dates, filters, start_bit, end_bit, twos_complement, exclude_masked)
        if len(data) == 0:
            return np.nan
        else:
            return np.min(data)
    
    def get_max(self, UT_region, register, dates = None, filters = "", start_bit = None, end_bit = None, twos_complement=False, exclude_masked = True):
        
        data = self.get_data(UT_region, register, dates, filters, start_bit, end_bit, twos_complement, exclude_masked)
        if len(data) == 0:
            return np.nan
        else:
            return np.max(data)
    
    def get_median(self, UT_region, register, dates = None, filters = "", start_bit = None, end_bit = None, twos_complement=False, exclude_masked = True):
        
        data = self.get_data(UT_region, register, dates, filters, start_bit, end_bit, twos_complement, exclude_masked)
        
        return np.median(data)
    
    def get_lower_quartile(self, UT_region, register, dates = None, filters = "", start_bit = None, end_bit = None, twos_complement=False, exclude_masked = True):
        
        data = self.get_data(UT_region, register, dates, filters, start_bit, end_bit, twos_complement, exclude_masked)
        if len(data) == 0:
            return np.nan
        else:
            return np.quantile(data,0.25)
    
    def get_upper_quartile(self, UT_region, register, dates = None, filters = "", start_bit = None, end_bit = None, twos_complement=False, exclude_masked = True):
        
        data = self.get_data(UT_region, register, dates, filters, start_bit, end_bit, twos_complement, exclude_masked)
        if len(data) == 0:
            return np.nan
        else:
            return np.quantile(data,0.75)
    
    def get_lower_whisker(self, UT_region, register, dates = None, filters = "", start_bit = None, end_bit = None, twos_complement=False, exclude_masked = True):
        
        data = np.array(self.get_data(UT_region, register, dates, filters, start_bit, end_bit, twos_complement, exclude_masked))
        
        if len(data) == 0:
            return np.nan
        else:
            Q1 = np.quantile(data,0.25)
            Q3 = np.quantile(data,0.75)
            IQR = Q3-Q1
            ideal = Q1 - 1.5*IQR
            
            return data[data>=ideal].min()
    
    def get_upper_whisker(self, UT_region, register, dates = None, filters = "", start_bit = None, end_bit = None, twos_complement=False, exclude_masked = True):
        
        data = np.array(self.get_data(UT_region, register, dates, filters, start_bit, end_bit, twos_complement, exclude_masked))
        
        if len(data) == 0:
            return np.nan
        else:
            Q1 = np.quantile(data,0.25)
            Q3 = np.quantile(data,0.75)
            IQR = Q3-Q1
            ideal = Q3 + 1.5*IQR
            
            return data[data<=ideal].max()
    
        
    def get_outliers(self, UT_region, register, dates = None, filters = "", start_bit = None, end_bit = None, twos_complement=False):
        
        
        
        data = self.get_data_location(UT_region, register, dates, filters, start_bit, end_bit, twos_complement)
        
        upper_whisker = self.get_upper_whisker(UT_region, register, dates, filters, start_bit, end_bit, twos_complement)
        lower_whisker = self.get_lower_whisker(UT_region, register, dates, filters, start_bit, end_bit, twos_complement)
        
        outliers = {}
        for date, date_dict in data.items():
            outliers[date] = {}
            for key, value in date_dict.items():
                if np.shape(value) == ():
                    if (value > upper_whisker or value < lower_whisker):
                        outliers[date][key] = value
                else:
                    channel_outliers = {}
                    for i in range(128):
                        if (value[i] > upper_whisker or value[i] < lower_whisker):
                            channel_outliers[i] = value[i]
                    if len(channel_outliers) > 0:
                        outliers[date][key] = channel_outliers
                    
        return outliers
    
    def get_difference(self, UT_region, register, date1, date2, filters = "", start_bit = None, end_bit = None, twos_complement = False):
        
        ret = 0
        data_from_date1 = {}
        data_from_date2 = {}
        
        try:
            data_from_date1 = self.get_data_location(UT_region, register, date1, filters, start_bit, end_bit, twos_complement)
        except ValueError:
            ret = np.nan
        try:
            data_from_date2 = self.get_data_location(UT_region, register, date2, filters, start_bit, end_bit, twos_complement)
        except ValueError:
            ret = np.nan
            
        diff_dict = {}
        if math.isnan(ret):
            diff_dict[UT_region] = ret
        else:
            list_of_keys = []
            for key in data_from_date1[date1].keys():
                list_of_keys.append(key)
            for key in data_from_date2[date2].keys():
                if key not in list_of_keys:
                    list_of_keys.append(key)
                    
            for key in list_of_keys:
                if (key in data_from_date1[date1].keys()) and (key in data_from_date2[date2].keys()):
                    diff_dict[key] = np.array(data_from_date2[date2][key]) - np.array(data_from_date1[date1][key])
                else:
                    diff_dict[key] = np.nan
                
        

            
        return diff_dict
    
    def plot_trend(self, ECS, register, dates = None, filters = "", start_bit = None, end_bit = None, twos_complement=False, fig = None, ax = None, **kwargs):
        
        if dates == None:
            dates = self.dates
            
        if np.shape(dates) == ():
            dates = [dates]
    
        nruns = len(dates)
        if register in self.vector_regs:
            flag = "vector"
            if (len(ECS.split(".")) != 3):
                raise Exception("For vector registers, the ECS has to be specified properly for a given channel, e.g.: UTaX_1CT_M1E.Chip0.Ch0")
        else:
            flag = "non_vector"
            if (len(ECS.split(".")) != 2):
                raise Exception("For non-vector registers, the ECS has to be specified properly for a given chip, e.g.: UTaX_1CT_M1E.Chip0")
        
        if flag == "vector":
            UT_sector, UT_chip, UT_channel = ECS.split(".")
            UT_channel = int(UT_channel[2:])
            if 'A' in UT_sector.split("_")[1]: UT_side = 'UADAQFEE'
            elif 'C' in UT_sector.split("_")[1]: UT_side = 'UCDAQFEE' 
            y = [get_bits(int(self.at_date(date)[UT_side][UT_sector][UT_chip][register][UT_channel], 16), start_bit, end_bit, twos_complement) for date in dates]
        
        else:
            UT_sector, UT_chip = ECS.split(".")
            if 'A' in UT_sector.split("_")[1]: UT_side = 'UADAQFEE'
            elif 'C' in UT_sector.split("_")[1]: UT_side = 'UCDAQFEE' 
            y = [get_bits(int(self.at_date(date)[UT_side][UT_sector][UT_chip][register], 16), start_bit, end_bit, twos_complement) for date in dates]
        
        timestamps = []
        for date in dates:
            timestamps.append(date.replace(" ", "\n"))
        
        if (fig == None and ax == None):
            fig, ax = plt.subplots()
        x = range(nruns)
        ax.set_xticks(range(nruns))
        ax.set_xticklabels(timestamps)
        ax.plot(x,y, 'o-', **kwargs)
        ax.set_xlabel("Date")
        ax.set_ylabel(register)
        ax.set_title(f"Trend of {register} for {ECS}")
        ax.grid()
        fig.tight_layout()
        
        return fig, ax
    
    def plot_hist(self, UT_region, register, dates = None,  filters = "", start_bit=None, end_bit=None, twos_complement=False, fig = None, ax = None, **kwargs):
        
        data = np.array(self.get_data(UT_region, register, dates, filters, start_bit, end_bit, twos_complement)).flatten()
        
        if (fig == None and ax == None):
            fig, ax = plt.subplots()
        
        ax.hist(data, **kwargs)
        
        ax.set_xlabel(register)
        ax.set_ylabel("Entries")
        ax.grid(ls=":")
        ax.set_title(f"{register} ({UT_region})")
        fig.tight_layout()
        
        return fig, ax

    def plot_hist_evolution(self, UT_region, register, dates = None,  filters = "", start_bit=None, end_bit=None, twos_complement=False, **kwargs):    
        
        if dates == None:
            dates = self.dates
            
        if np.shape(dates) == ():
            dates = [dates]
        
        
        # if register in self.vector_regs:
        #     raise ValueError(f"register: '{register}' not supported yet!")
        
        register_values = [[] for i in range(len(dates))]
        
        nrows = math.ceil(self.nrecipes / 4)

        # Create a grid of subplots with nrows rows and 4 columns
        fig, axs = plt.subplots(nrows, 4, figsize=(20, 5 * nrows))

        # Flatten the axs array to make it easier to iterate over
        axs = axs.flatten()
        
        for t, date in enumerate(dates):
            
            ax = axs[t]
            register_values[t] = self.get_data(UT_region = UT_region, register = register, dates = date, 
                                               filters = filters, start_bit = start_bit, end_bit = end_bit,
                                               twos_complement=twos_complement)
            
            register_values[t] = np.array(register_values[t]).flatten()
            
            timestamp = date
            ax.set_title(register + f" ({UT_region})" + "\n" + timestamp)
            ax.hist(register_values[t], **kwargs, color='navy')
            ax.set_xlabel("Register value")
            ax.set_ylabel("Counts")
        fig.tight_layout()
        fig.set_facecolor("white")
        for i in range(len(dates), nrows * 4):
            axs[i].axis('off')
        return  fig, axs, register_values
    
    def _get_box_plot_data(self, dates, bp):
        rows_list = []

        for i in range(len(dates)):
            dict1 = {}
            dict1['date'] = dates[i]
            dict1['lower_whisker'] = bp['whiskers'][i*2].get_ydata()[1]
            dict1['lower_quartile'] = bp['boxes'][i].get_path().vertices[0,1]
            dict1['median'] = bp['medians'][i].get_ydata()[1]
            dict1['upper_quartile'] = bp['boxes'][i].get_path().vertices[2,1]
            dict1['upper_whisker'] = bp['whiskers'][(i*2)+1].get_ydata()[1]
            rows_list.append(dict1)

        return pd.DataFrame(rows_list)
    
    def plot_boxplot(self, UT_region, register, dates = None,  filters = "", start_bit=None, end_bit=None, twos_complement=False, fig = None, ax = None, **kwargs):
        
        data = np.array(self.get_data(UT_region, register, dates, filters, start_bit, end_bit, twos_complement)).flatten()
        
        if (fig == None and ax == None):
            fig, ax = plt.subplots()
        
        bplot = ax.boxplot(data, patch_artist= True, **kwargs)
        for patch in bplot['boxes']:
            patch.set_facecolor("pink")
        
        rows_list = []
        dict1 = {}
        dict1['lower_whisker'] = bplot['whiskers'][0].get_ydata()[1]
        dict1['lower_quartile'] = bplot['boxes'][0].get_path().vertices[0,1]
        dict1['median'] = bplot['medians'][0].get_ydata()[1]
        dict1['upper_quartile'] = bplot['boxes'][0].get_path().vertices[2,1]
        dict1['upper_whisker'] = bplot['whiskers'][1].get_ydata()[1]
        rows_list.append(dict1)

        df = pd.DataFrame(rows_list)
        
        
        ax.set_xlabel("")
        ax.set_ylabel(register)
        ax.grid()
        ax.set_title(f"{register} ({UT_region})")
        fig.tight_layout()
        
        return fig, ax, df
    
    def plot_boxplot_trend(self, UT_region, register, dates = None,  filters = "", start_bit=None, end_bit=None, twos_complement=False, fig = None, ax = None, **kwargs):    
        
        if dates == None:
            dates = self.dates
            
        if np.shape(dates) == ():
            dates = [dates]
        
        if (fig == None and ax == None):
            fig, ax = plt.subplots()
        all_data = [np.array(self.get_data(UT_region, register, date, filters, start_bit, end_bit, twos_complement)).flatten() for date in dates]
        timestamps = []
        for date in dates:
            timestamps.append(date.replace(" ", "\n"))
        
        
        
        bplot = ax.boxplot(all_data, patch_artist= True, labels=timestamps, **kwargs)
        for patch in bplot['boxes']:
            patch.set_facecolor("pink")
        ax.set_ylabel(register)
        ax.set_xlabel("Date")
        ax.grid()
        
        df = self._get_box_plot_data(dates, bplot)
        
        return fig, ax, df, bplot
    
    def plot_waterfall(self, UT_region, register, dates = None, filters = "", start_bit=None, end_bit=None, twos_complement = False, fig = None, ax = None, **kwargs):    
        
        if dates == None:
            dates = self.dates
            
        if np.shape(dates) == ():
            dates = [dates]
        
        cond_dict = {}
            
        if filters != "":
            for cond in filters.split(","):
                cond_tab = cond.split("==")
                key = cond_tab[0].replace(" ", "")
                val = cond_tab[1].replace(" ", "")
                
                val = val.split("|")
                cond_dict[key] = val
        
        if (fig == None and ax == None):
            fig, ax = plt.subplots()
        
        register_values = [[] for i in range(len(dates))]

        if ((len(UT_region) == 7) and ("_" in UT_region) and np.any([(plane in UT_region) for plane in self.UT_planes])):
            
            if register in self.vector_regs:
                raise ValueError(f"register: '{register}' not supported yet for staves! You can plot it for a given chip")

            for t, date in enumerate(dates):
                
                stave_labels = []
                
                UT_dict = self.data[t]
                UT_plane, UT_stave = UT_region.split("_")
                if 'A' in UT_stave: UT_side = 'UADAQFEE'
                elif 'C' in UT_stave: UT_side  = 'UCDAQFEE'
                for UT_sector in UT_dict[UT_side].keys():
                    if UT_stave in UT_sector and UT_plane in UT_sector:
                        for UT_chip in UT_dict[UT_side][UT_sector].keys():
                            ECS = UT_sector + "." + UT_chip
                            if(self._check_filters(ECS, cond_dict)):
                                stave_labels.append(UT_sector.split("_")[-1])
                                UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                if len(UT_value) <= 2:
                                    register_values[t].append(get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement))
                            
            x_max = len(register_values[t])
            y_max = len(register_values)
            
            register_values = np.array(register_values, dtype=float)
            im = ax.imshow(register_values, origin = 'lower', aspect = 'auto', extent=(0,x_max, -0.5, y_max - 0.5), interpolation = 'nearest', **kwargs)
            
            ax.set_title(register + f" ({UT_region})", y = 1.05)
            ax.set_xlabel("ASIC")
            x_major_ticks = np.arange(0,len(register_values[t]), 4)
            x_minor_ticks = np.arange(len(register_values[t]))
            ax.set_xticks(x_major_ticks, labels = [])
            ax.set_xticks(x_minor_ticks, labels = stave_labels, minor = True, rotation = 45)
            
            i = 0
            for label in ax.xaxis.get_ticklabels(minor=True):
                if i%3 != 1:
                    label.set_visible(False)
                i+=1


        ############################################# Vector registers per chip ################################################

        elif ((".Chip" in UT_region) and np.any([(plane in UT_region) for plane in self.UT_planes])):
            
            if register not in self.vector_regs:
                raise ValueError(f"Plotting non-vector register: '{register}' for a given chip is pointless!")

            for t, date in enumerate(dates):
                
                stave_labels = []
                
                UT_sector, UT_chip = UT_region.split(".")
                UT_dict = self.data[t]
                for UT_side in UT_dict.keys():
                    if UT_sector in UT_dict[UT_side].keys():
                        ECS = UT_sector + "." + UT_chip
                        if(self._check_filters(ECS, cond_dict)):
                            UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                            register_values[t] = [get_bits(int(val, 16), start_bit, end_bit) for val in UT_value[::-1]]
            
            x_max = len(register_values[t])
            y_max = len(register_values)
            
            register_values = np.array(register_values, dtype=float)
            im = ax.imshow(register_values, origin = 'lower', aspect = 'auto', extent=(0,x_max, -0.5, y_max - 0.5), interpolation = 'nearest', **kwargs)
            
            ax.set_title(register + f" ({UT_region})", y = 1.05)
            ax.set_xlabel("CHANNEL")
            
        else:
            raise Exception(f"Wrong or not implemented UT region: {UT_region}")
            
        ax.set_ylabel("TIMESTAMP")
        timestamps = [date.split(" ")[0] + "\n" + ''.join(date.split(" ")[1:]) for date in dates]
        y_major_ticks = np.arange(0, y_max, 1)
        y_minor_ticks = np.arange(-0.5, y_max-0.5, 1)
        ax.set_yticks(y_major_ticks, labels = timestamps)
        ax.set_yticks(y_minor_ticks, minor = True)
        ax.grid(axis = 'y', which = 'minor', lw = 3)
        
        fig.colorbar(im, ax=ax)
        fig.set_facecolor("white")
        return fig, ax, im, register_values
            

    def plot_UT_plane(self, UT_plane, register, dates = None, option = None, draw_per_channel = False,
                      fs = "default", start_bit=None, end_bit=None, twos_complement=False,
                      side = None, mirroring = True, masking = True, **kwargs):
        
        
        max_vmax_value = np.nan
        if "vmax" in kwargs.keys():
            if type(kwargs["vmax"]) == str:
                if "max/" in kwargs["vmax"]:
                    max_vmax_value = int(kwargs["vmax"].split("/")[1])
        
        if register in self.vector_regs and option == None:
            if not draw_per_channel:
                raise ValueError("option None is supported only for draw_per_channel = True!")
        
        if register not in self.vector_regs and (option != None) and (option !="differences"):
            raise ValueError(f"Only option == None is supported for non-vector registers, like: '{register}'!")
        
        if draw_per_channel:
            if option != None:
                raise ValueError("Only option None is supported for draw_per_channel = True!")
        
        if dates == None:
            dates = self.dates
        
        if np.shape(dates) == ():
            dates = [dates]
            if (UT_plane != None) and UT_plane not in self.UT_planes:
                raise ValueError(f"Unknown UT-plane: '{UT_plane}'!")
        else:
            if UT_plane not in self.UT_planes:
                raise ValueError(f"Unknown UT-plane: '{UT_plane}'!")
        
        if mirroring:
            UT_map = self.UT_mirrored_map
        else:
            UT_map = self.UT_map
            
        if fs == "default":
            fs = (15,10)
        
        #--------------------------------------UT tab declaration-----------------------------------------#
        
        if draw_per_channel:
            UT_tab = np.empty((28,144*128)) 
        else:
            UT_tab = np.empty((28,144)) 
        
        UT_tabs = []
        
        
        ################################################ 2D maps drawing options ###############################################
        allowed_options = ["mean", "std", "rms", "min", "max", "median", "upper_quartile", "lower_quartile", "upper_whisker", 
                           "lower_whisker", "outliers", "outliers_plane", "outliers_sensor_type", 
                           "outliers_plane_sensor_type", "outliers_chip", "differences"]
        
        if option is not None and option not in allowed_options:
            raise ValueError(f"Unknown option: '{option}'! Allowed options: {allowed_options}")
        else:
            if option == "mean": func = self.get_mean
            elif option == 'std': func = self.get_std
            elif option == 'rms': func = self.get_rms
            elif option == "min": func = self.get_min
            elif option == "max": func = self.get_max
            elif option == "median": func = self.get_median
            elif option == "upper_quartile": func = self.get_upper_quartile
            elif option == "lower_quartile": func = self.get_lower_quartile
            elif option == "upper_whisker": func = self.get_upper_whisker
            elif option == "lower_whisker": func = self.get_lower_whisker

        cbars = []
            
        if UT_plane == None:
            fig, axs = plt.subplots(2,2, figsize = (fs[0]*2, fs[1]*2))
            axs = axs.flatten()
            for i, plane in enumerate(self.UT_planes):
                date = dates[0]
                UT_dict = self.at_date(date)
                UT_tab[:] = np.nan
                
                if option == None:
                    for UT_side in UT_dict.keys():
                        for UT_sector in UT_dict[UT_side].keys():
                            if plane in UT_sector:
                                for UT_chip in UT_dict[UT_side][UT_sector].keys():
                                    if register in UT_dict[UT_side][UT_sector][UT_chip].keys():
                                        UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                        masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]
                                        if (masking and (masks != (128*[1]))) or not masking:
                                            map_vals = UT_map[UT_sector[5:]][UT_chip]
                                            if len(UT_value) <= 2:          #dodatkowe zabezpieczenie przed vector registers
                                                for val in map_vals:
                                                    UT_tab[val] = get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement)  # Convert UT_value to int and get the desired bits
                                            else:
                                                for val in map_vals:
                                                    UT_tab[val[0], val[1]*128:(1+val[1])*128] = [get_bits(int(UT_value[i], 16), start_bit, end_bit, twos_complement) if masks[i] == 0 else np.nan for i in range(len(UT_value))]
                                                        
                    title = ""
                
                elif option not in ["outliers", "outliers_plane", "outliers_sensor_type", "outliers_plane_sensor_type", "outliers_chip", "differences"]:
                    for UT_side in UT_dict.keys():
                        for UT_sector in UT_dict[UT_side].keys():
                            if plane in UT_sector:
                                for UT_chip in UT_dict[UT_side][UT_sector].keys():
                                    UT_region = UT_sector + "." + UT_chip
                                    if register in UT_dict[UT_side][UT_sector][UT_chip].keys():
                                        if masking == True:
                                            UT_value = func(UT_region, register, date, "", start_bit, end_bit, twos_complement)
                                        else:
                                            UT_value = func(UT_region, register, date, "", start_bit, end_bit, twos_complement)
                                        if (masking and (UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"] != (128*[1]))) or not masking:
                                            map_vals = UT_map[UT_sector[5:]][UT_chip]
                                            for val in map_vals:
                                                UT_tab[val] = UT_value              
            
                    title = "(" + option + " for chips)"
                    
                elif option == "outliers_chip":
                    for UT_side in UT_dict.keys():
                        for UT_sector in UT_dict[UT_side].keys():
                            if plane in UT_sector:
                                for UT_chip in UT_dict[UT_side][UT_sector].keys():
                                    UT_region = UT_sector + "." + UT_chip
                                    if register in UT_dict[UT_side][UT_sector][UT_chip].keys():
                                        outliers = self.get_outliers(UT_region, register, date, "", start_bit, end_bit, twos_complement)
                                        for key, value in outliers[date].items():
                                            UT_sector, UT_chip, = key.split(".")
                                            if (masking and (UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"] != (128*[1]))) or not masking:
                                                map_vals = UT_map[UT_sector[5:]][UT_chip]
                                                for val in map_vals:
                                                    if math.isnan(UT_tab[val]):
                                                        UT_tab[val] = 0
                                                    if type(value) != dict:
                                                            UT_tab[val] += 1
                                                    else:
                                                        UT_tab[val] += (len(value))
                    title = "(outliers wrt. chips)"
                    
                elif option == "differences":
                    for UT_side in UT_dict.keys():
                        for UT_sector in UT_dict[UT_side].keys():
                            if plane in UT_sector:
                                for UT_chip in UT_dict[UT_side][UT_sector].keys():
                                    UT_region = UT_sector + "." + UT_chip
                                    if register in UT_dict[UT_side][UT_sector][UT_chip].keys():
                                        UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                        if len(UT_value) > 2:  #For vector registers:
                                            dec_UT_value = [get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value]
                                            UT_value = np.mean(dec_UT_value)
                                            title = "(mean values)"
                                        else:
                                            title = "(values)"
                                            UT_value = get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement)
                                        if (masking and (UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"] != (128*[1]))) or not masking:
                                            map_vals = UT_map[UT_sector[5:]][UT_chip]
                                            for val in map_vals:
                                                UT_tab[val] = UT_value   
                                            
                else:
                    if option == "outliers":
                        outliers = self.get_outliers("UT", register, date, "", start_bit, end_bit, twos_complement)[date]
                        title = "(outliers wrt. whole UT)"
                    elif option == "outliers_plane":
                        outliers = self.get_outliers(plane, register, date, "", start_bit, end_bit, twos_complement)[date]
                        title = "(outliers wrt. the plane)"
                    elif option == "outliers_sensor_type":
                        outliers_A = self.get_outliers("UT", register, date, "sensor_type == A", start_bit, end_bit, twos_complement)[date]
                        outliers_B = self.get_outliers("UT", register, date, "sensor_type == B", start_bit, end_bit, twos_complement)[date]
                        outliers_C = self.get_outliers("UT", register, date, "sensor_type == C", start_bit, end_bit, twos_complement)[date]
                        outliers_D = self.get_outliers("UT", register, date, "sensor_type == D", start_bit, end_bit, twos_complement)[date]
                        outliers = outliers_A | outliers_B | outliers_C | outliers_D
                        title = "(outliers wrt. sensor type for whole UT)"
                    elif option == "outliers_plane_sensor_type":
                        outliers_A = self.get_outliers(plane, register, date, "sensor_type == A", start_bit, end_bit, twos_complement)[date]
                        outliers_B = self.get_outliers(plane, register, date, "sensor_type == B", start_bit, end_bit, twos_complement)[date]
                        outliers_C = self.get_outliers(plane, register, date, "sensor_type == C", start_bit, end_bit, twos_complement)[date]
                        outliers_D = self.get_outliers(plane, register, date, "sensor_type == D", start_bit, end_bit, twos_complement)[date]
                        outliers = outliers_A | outliers_B | outliers_C | outliers_D
                        title = "(outliers wrt. sensor type for the plane)"
                    
                    n_outliers = 0
                    for key, value in outliers[date].items():
                        UT_hybrid, UT_chip = key.split(".")
                        if (masking and (UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"] != (128*[1]))) or not masking:
                            if plane in UT_hybrid:
                                map_vals = UT_map[UT_hybrid[5:]][UT_chip]
                                n_outliers += len(value)
                                for val in map_vals:
                                    if math.isnan(UT_tab[val]):
                                        UT_tab[val] = 0
                                    UT_tab[val] += len(value)
                    print(f"{date}, no. of outliers: {n_outliers}")            
                                        
                
                if math.isnan(max_vmax_value) == False:
                    kwargs["vmax"] = np.min([np.nanmax(UT_tab), max_vmax_value]) 

            
                fig, axs[i], im, cbar = draw_UT(UT_tab, plane, fig, axs[i], side, **kwargs)

                cbars.append(cbar)
                
                
                timestamp = date
                if side == None:
                    axs[i].set_title(plane + "  " + register + " " + title + "\n" + timestamp)
                else:
                    axs[i].set_title(plane + f" ({side}-side)" + "  " + register + " " + title + "\n" + timestamp)
                
                UT_tabs.append(UT_tab)
             
        else:  #(UT_plane != None)
            
            if side == None:
                num_cols = np.min([2,len(dates)]).astype(int)
            else:
                num_cols = np.min([3,len(dates)]).astype(int)
            num_rows = np.ceil(len(dates)/ num_cols).astype(int)
            fig, axs = plt.subplots(num_rows, num_cols, figsize=(fs[0]*num_cols, fs[1]*num_rows))
            if type(axs) != np.ndarray:
                axs = np.array([axs])
            axs = axs.flatten()  # Flatten the array to easily iterate over it
            
            for t, date in enumerate(dates):
                UT_dict = self.at_date(date)
                UT_tab[:] = np.nan
                
                
                if option == None:
                    for UT_side in UT_dict.keys():
                        for UT_sector in UT_dict[UT_side].keys():
                            if UT_plane in UT_sector:
                                for UT_chip in UT_dict[UT_side][UT_sector].keys():
                                    if register in UT_dict[UT_side][UT_sector][UT_chip].keys():
                                        UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                        masks = UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"]
                                        if (masking and (masks != (128*[1]))) or not masking:
                                            map_vals = UT_map[UT_sector[5:]][UT_chip]
                                            if len(UT_value) <= 2:          #dodatkowe zabezpieczenie przed vector registers
                                                for val in map_vals:
                                                    UT_tab[val] = get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement)  # Convert UT_value to int and get the desired bits
                                            else:
                                                for val in map_vals:
                                                    UT_tab[val[0], val[1]*128:(1+val[1])*128] = [get_bits(int(UT_value[i], 16), start_bit, end_bit, twos_complement) if masks[i] == 0 else np.nan for i in range(len(UT_value))]
                    title = ""
                
                elif option not in ["outliers", "outliers_plane", "outliers_sensor_type", "outliers_plane_sensor_type", "outliers_chip", "differences"]:
                    for UT_side in UT_dict.keys():
                        for UT_sector in UT_dict[UT_side].keys():
                            if UT_plane in UT_sector:
                                for UT_chip in UT_dict[UT_side][UT_sector].keys():
                                    UT_region = UT_sector + "." + UT_chip
                                    if register in UT_dict[UT_side][UT_sector][UT_chip].keys():
                                        UT_value = func(UT_region, register, date, "", start_bit, end_bit, twos_complement)
                                        if (masking and (UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"] != (128*[1]))) or not masking:
                                            map_vals = UT_map[UT_sector[5:]][UT_chip]
                                            for val in map_vals:
                                                UT_tab[val] = UT_value              
            
                    title = "(" + option + " for chips)"
                    
                elif option == "outliers_chip":
                    for UT_side in UT_dict.keys():
                        for UT_sector in UT_dict[UT_side].keys():
                            if UT_plane in UT_sector:
                                for UT_chip in UT_dict[UT_side][UT_sector].keys():
                                    UT_region = UT_sector + "." + UT_chip
                                    if register in UT_dict[UT_side][UT_sector][UT_chip].keys():
                                        outliers = self.get_outliers(UT_region, register, date, "", start_bit, end_bit, twos_complement)
                                        for key, value in outliers[date].items():
                                            UT_sector, UT_chip, = key.split(".")
                                            if (masking and (UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"] != (128*[1]))) or not masking:
                                                map_vals = UT_map[UT_sector[5:]][UT_chip]
                                                for val in map_vals:
                                                    if math.isnan(UT_tab[val]):
                                                        UT_tab[val] = 0
                                                    if type(value) != dict:
                                                            UT_tab[val] += 1
                                                    else:
                                                        UT_tab[val] += (len(value))
                    title = "(outliers wrt. chips)"
                    
                elif option == "differences":
                    for UT_side in UT_dict.keys():
                        for UT_sector in UT_dict[UT_side].keys():
                            if UT_plane in UT_sector:
                                for UT_chip in UT_dict[UT_side][UT_sector].keys():
                                    UT_region = UT_sector + "." + UT_chip
                                    if register in UT_dict[UT_side][UT_sector][UT_chip].keys():
                                        if t == 0:
                                            UT_value = UT_dict[UT_side][UT_sector][UT_chip][register]
                                            if len(UT_value) > 2:  #For vector registers:
                                                dec_UT_value = [get_bits(int(val, 16), start_bit, end_bit, twos_complement) for val in UT_value]
                                                UT_value = np.mean(dec_UT_value)
                                                title = "(mean values)"
                                            else:
                                                title = "(values)"
                                                UT_value = get_bits(int(UT_value, 16), start_bit, end_bit, twos_complement)
                                        else:
                                            diff_dict = self.get_difference(UT_region, register, dates[t-1], dates[t], "", start_bit, end_bit, twos_complement)
                                            if np.shape(diff_dict[UT_region]) == ():
                                                if math.isnan(diff_dict[UT_region]):
                                                    UT_value = np.nan
                                                else:
                                                    UT_value = diff_dict[UT_region]
                                                title = "(differences)"
                                            else:
                                                UT_value = np.sum(np.where(diff_dict[UT_region] != 0, 1, 0))
                                                title = "(number of differences)"
                                        
                                        if (masking and (UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"] != (128*[1]))) or not masking:
                                            map_vals = UT_map[UT_sector[5:]][UT_chip]
                                            for val in map_vals:
                                                UT_tab[val] = UT_value   
                                        
                else:
                    if option == "outliers":
                        outliers = self.get_outliers("UT", register, date, "", start_bit, end_bit, twos_complement)[date]
                        title = "(outliers wrt. whole UT)"
                    elif option == "outliers_plane":
                        outliers = self.get_outliers(UT_plane, register, date, "", start_bit, end_bit, twos_complement)[date]
                        title = "(outliers wrt. the plane)"
                    elif option == "outliers_sensor_type":
                        outliers_A = self.get_outliers("UT", register, date, "sensor_type == A", start_bit, end_bit, twos_complement)[date]
                        outliers_B = self.get_outliers("UT", register, date, "sensor_type == B", start_bit, end_bit, twos_complement)[date]
                        outliers_C = self.get_outliers("UT", register, date, "sensor_type == C", start_bit, end_bit, twos_complement)[date]
                        outliers_D = self.get_outliers("UT", register, date, "sensor_type == D", start_bit, end_bit, twos_complement)[date]
                        outliers = outliers_A | outliers_B | outliers_C | outliers_D
                        title = "(outliers wrt. sensor type for whole UT)"
                    elif option == "outliers_plane_sensor_type":
                        outliers_A = self.get_outliers(UT_plane, register, date, "sensor_type == A", start_bit, end_bit, twos_complement)[date]
                        outliers_B = self.get_outliers(UT_plane, register, date, "sensor_type == B", start_bit, end_bit, twos_complement)[date]
                        outliers_C = self.get_outliers(UT_plane, register, date, "sensor_type == C", start_bit, end_bit, twos_complement)[date]
                        outliers_D = self.get_outliers(UT_plane, register, date, "sensor_type == D", start_bit, end_bit, twos_complement)[date]
                        outliers = outliers_A | outliers_B | outliers_C | outliers_D
                        title = "(outliers wrt. sensor type for the plane)"
                    
                    n_outliers = 0
                    for key, value in outliers[date].items():
                        UT_hybrid, UT_chip = key.split(".")
                        if (masking and (UT_dict[UT_side][UT_sector][UT_chip]["masks_cfg"] != (128*[1]))) or not masking:
                            if UT_plane in UT_hybrid:
                                map_vals = UT_map[UT_hybrid[5:]][UT_chip]
                                n_outliers += len(value)
                                for val in map_vals:
                                    if math.isnan(UT_tab[val]):
                                        UT_tab[val] = 0
                                    UT_tab[val] += len(value)
                    print(f"{date}, no. of outliers: {n_outliers}")            
                                        
                
                if math.isnan(max_vmax_value) == False:
                    kwargs["vmax"] = np.min([np.nanmax(UT_tab), max_vmax_value]) 

            
                fig, axs[t], im, cbar = draw_UT(UT_tab, UT_plane, fig, axs[t], side, **kwargs)

                cbars.append(cbar)
                
                
                timestamp = date
                if side == None:
                    axs[t].set_title(UT_plane + "  " + register + " " + title + "\n" + timestamp)
                else:
                    axs[t].set_title(UT_plane + f" ({side}-side)" + "  " + register + " " + title + "\n" + timestamp)
                
                UT_tabs.append(UT_tab)
            
            # If there are less plots than subplots, hide the remaining ones
            for t in range(len(dates), len(axs)):
                axs[t].axis('off')

        fig.set_facecolor("white")
        #plt.show()
        
        return fig, axs, UT_tabs, cbars




