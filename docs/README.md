# Analysis of SALT registers - tutorial and documentation

# Getting started

My own analysis is available in `RegisterEvolution.ipynb` file - you can have a look and inspire yourself!

Make sure you have access to the environment with all necessary Python packages:
- numpy
- pandas
- matplotlib
- pickle
- json
- re
- math

There are also some packages which are not required, but may come in handy:
- os
- copy

All necessary definitions are stored in `UT_Helper.py`, so make sure to start your own notebook with:

```python
from UT_Helper import *

```

At the beginning of your analysis create an object of the `UT_evolution` class. Its only argument is a `list` containing paths to register dump files:

```python
RECIPES = [<filepath1>, <filepath2>, ...]
UT_ev = UT_evolution(RECIPES)
```

The dump files can contain data from one of the UT-sides or from the whole detector (if they are manually merged - paste C-side file contents at the end of the A-side file). Their names should follow the naming schemes:

```
# For the UA/UC dump file
<name>_<date with format YYYYMMDDTHHMMSS>.txt/json
# e.g. register_dump_20240304T124152.txt/json


# For the manually merged UT dump file:
<name>_<date: YYYYMMDD A-side: THHMMSS C-side:  THHMMSS> 
# e.g. register_dump_20240304T124152T124208.txt/json
```

This is a preliminary version of this programme so it's not well-optimised. Reading the data might take some time: 2-3s per one file. 

Data are stored in a `list` of nested `dicts`, one `dict` per file. Each `dict` has the following structure 
```python
{'UADAQFEE':
    {'UTaX_1AT_M1E':
        {'Chip0':
            {
                <register name>: <register value>,
                ...
            }
         'Chip1':
         ...
        },
     'UTaX_1AT_M1W':
     ...
    },
 'UCDAQFEE':  
 ...
 }
 ```


# Tutorial

After creating an object of `UT_evolution` class, you can access the data loaded from a given file. You can use two different functions - `UT_evolution.at()` to get the data by its index (0 for the file with earliest date in the name, 1 for the next one and so on) or `UT_evolution.at_date()` to get the data by the timestamp:

```python
# to access the dict for the first file
UT_ev.at(0)

# to access the dict for the file with the data from one UT_side, e.g: register_dump_20240405T182158.txt

UT_ev.at_date('2024/04/05 18:21:58')

# to access the dict for the file with the data from the whole UT, e.g.:  register_dump_20240405T182211T182158.txt

UT_ev.at_date('2024/04/05 18:22:11, 18:21:58') 

```

You can access the values for the given side, sector, Chip or individual register:

```python
# To get the data for a given side of the UT:

UT_ev.at(0)["UCDAQFEE"]

# To get the data for a given sector of the UT:

UT_ev.at(0)["UCDAQFEE"]["UTaX_1CT_M1E"]

# To get the data for a given chip of the UT:

UT_ev.at(0)["UCDAQFEE"]["UTaX_1CT_M1E"]["Chip0"]

# To get the data for a given register of the UT chip:

UT_ev.at(0)["UCDAQFEE"]["UTaX_1CT_M1E"]["Chip0"]["adc_clk_cfg"]

```

Using `dicts` to have a look as individual value is quick and convenient. However, if we want to get a huge chunk of data and plot a histogram for example, it is much better to have a `list`. That's when `UT_evolution.get_data()` comes in handy. This function lets you specify the region of the UT that you want to get the data from, the dates, the name of the register, some additional conditions called `"filters"`. If the registers are represented by binary numbers, you can read only some of the bits and specify if the value should be read as signed or unsigned:

```python
# Get ZS threshold encoded as 4-bit unsigned number in 'n_zs_cfg' register. Data from the B-type sensors in UTaX plane, only from the first timestamp.

UT_ev.get_data(UT_region = "UTaX", 
               register = "n_zs_cfg", 
               dates = UT_ev.dates[0], 
               filters = "sensor_type == B", 
               start_bit = 0, 
               end_bit = 4, 
               twos_complement=False)

```

Sometimes you may want to preserve the information about the chip which the data comes from. In that case, you may use `UT_evolution.get_data_location()` in a similar way and you'll obtain a nested `dict` where the keys will store information about the dates and chips and the values will be the register values. 

```python
# Get the same values of ZS thresholds, but with additional information about the data location (ECS) and dates:

UT_ev.get_data_location(UT_region = "UTaX", 
               register = "n_zs_cfg", 
               dates = UT_ev.dates[0], 
               filters = "sensor_type == B", 
               start_bit = 0, 
               end_bit = 4, 
               twos_complement=False)

```

There is a set of functions to retrieve a few basic characteristics of the dataset:
- `UT_evolution.get_mean()`
- `UT_evolution.get_min()`
- `UT_evolution.get_max()`
- `UT_evolution.get_median()`
- `UT_evolution.get_lower_quartile()`
- `UT_evolution.get_upper_quartile()`
- `UT_evolution.get_lower_whisker()`
- `UT_evolution.get_upper_whisker()`


Some of them are self-explanatory and the rest refers to the quantities used for making a boxplot. If you are not familiar with boxplot, take a look [here](https://en.wikipedia.org/wiki/Box_plot).

All the functions listed above need the same set of arguments as `UT_ev.get_data()` and `UT_ev.get_data_location()`. 

Single numbers however are often not enough to describe a dataset. In that case you may want to plot a histogram with `UT_evolution.plot_hist()`:

```python
UT_ev.plot_hist(UT_region = "UTaX", 
               register = "baselines_cfg", 
               dates = UT_ev.dates, 
               filters = "", 
               start_bit = 0, 
               end_bit = 7, 
               twos_complement=False)
```
<img src="1.png" alt="hist" width="400"/>

or even series of histograms, one for each timestamp with `UT_evolution.plot_hist_evolution()`:

```python
UT_ev.plot_hist_evolution(UT_region = "UTaX", 
               register = "baselines_cfg", 
               dates = UT_ev.dates, 
               filters = "", 
               start_bit = 0, 
               end_bit = 7, 
               twos_complement=False)

```
<img src="2.png" alt="hist evolution" width="1000"/>

You can also plot the boxplot:

```python
UT_ev.plot_boxplot(UT_region = "UTaX", 
               register = "baselines_cfg", 
               dates = UT_ev.dates, 
               filters = "", 
               start_bit = 0, 
               end_bit = 7, 
               twos_complement=False)

```
<img src="2b.png" alt="boxplot" width="400"/>


While plotting the boxplot trend with `UT_evolution.plot_boxplot_trend()` might be another way of tracking the evolution of a given dataset:

```python
UT_ev.plot_boxplot_trend(UT_region = "UTaX", 
               register = "baselines_cfg", 
               dates = UT_ev.dates, 
               filters = "", 
               start_bit = 0, 
               end_bit = 7, 
               twos_complement=False)
```
<img src="3.png" alt="boxplot" width="400"/>

To track the evolution of individual register value you may use `UT_evolution.plot_trend()`. In this case, you need to specify the *ECS* for the chip (or for the channel in case of vector registers) instead of the *UT_region*:

```python
UT_ev.plot_trend(ECS= "UTaX_1CT_M1E.Chip0",
                 register = "n_zs_cfg", 
                 dates = UT_ev.dates,#, 
                 filters = "", 
                 start_bit = 0, 
                 end_bit = 4, 
                 twos_complement=False)
```
<img src="4.png" alt="trend" width="400"/>

When you want to compare the evolution of registers across a stave or the evolution of vector registers across the chips, the `UT_evolution.plot_waterfall()` is there for you:

```python
UT_ev.plot_waterfall(UT_region= "UTaX_1CT_M1E.Chip0",
                 register = "baselines_cfg", 
                 dates = UT_ev.dates[:-1],#, 
                 filters = "", 
                 start_bit = 0, 
                 end_bit = 7, 
                 twos_complement=False)
```
<img src="5.png" alt="waterfall" width="400"/>

And finally, `UT_evolution.plot_UT_plane()` allows you to plot 2D maps of the UT to easily spot any anomalies and interesting patterns in the registers' distribution over the detecting planes. You can plot either the values of the registers defined per chip or some characteristics of the vector registers' distribution over chip, like mean, median etc:

```python
UT_ev.plot_UT_plane(UT_plane = 'UTbV', 
                    register = 'dll_vcdl_cfg', 
                    fs = (6,8),  #figure size
                    option = None, #just register values
                    dates = UT_ev.dates[:-1],
                    start_bit=0, 
                    end_bit=6, 
                    twos_complement = False,
                    side = "A")
```
<img src="6.png" alt="2D non-vector" width="1000"/>

```python
UT_ev.UT_ev.plot_UT_plane(UT_plane = 'UTaX', 
                    register = 'baselines_cfg', 
                    option = "mean",
                    dates = UT_ev.dates[:-2],
                    start_bit=0, 
                    end_bit=4,
                    twos_complement = False,
                    side = None)
```
<img src="7.png" alt="2D vector mean" width="1000"/>

If you want to analyse the outliers, 2D maps with a set of `"outliers ..."` options are there for you. Check out the docs below for more info. You may use make use of `UT_evolution.get_outliers()` function to get the location and values of the outliers. The outliers are defined according to the box plot, as those values lying below the lower whisker or above the upper whisker. 

```python
UT_ev.get_outliers(UT_region = 'UTaX_6AB_M3.Chip3', 
                     register = "baselines_cfg", 
                     dates = UT_ev.dates[0], 
                     filters = "", 
                     start_bit = None,
                     end_bit = None,
                     twos_complement = False)
```

Another important aspect of register analysis is the impact of SEUs. There are a few tools designed to help the user in that matter that monitor the differences between register values in the consecutive dumps. It's always good to have a look at 2D maps with option `"differences"`. This will allow you to spot the misbehaving chips. Then, you can use the `UT_evolution.get_difference()` function that returns a `dict` with differences between the register values at date2 and date1 (date2 - date1). 

```python
UT_ev.get_difference(UT_region = 'UTaX_6AB_M3.Chip3', 
                     register = "baselines_cfg", 
                     date1 = UT_ev.dates[-2], 
                     date2 = UT_ev.dates[-1], 
                     filters = "", 
                     start_bit = None,
                     end_bit = None,
                     twos_complement = False)
```

# Docs


## UT_evolution

### Constructor


`UT_evolution(`*recipes*`)`

**recipes**: `list` of `str`

> list containing paths to  register dump files / recipes with proper naming scheme

### Attributes and underlying data

- `UT_evolution.data` - `list` of nested `dicts`. Each `dict` contains data loaded from a given file

- `UT_evolution.dates` - `list` with all the dates in input files names 

- `UT_evolution.date_to_index` - `dict` with dates as keys and indices of `UT_evolution.data` as values

- `UT_evolution.recipes` - list containing paths to  register dump files / recipes 

- `UT_evolution.nrecipes` - number of files loaded to UT_evolution 

- `UT_evolution.UT_sides` - `["UADAQFEE", "UCDAQFEE"]` - for A-side and C-side respectively

- `UT_evolution.UT_planes` - `["UTaX", "UTbX", "UTaU", "UTbV"]`

- `UT_evolution.UT_sensor_types` - `["A-type", "B-type", "C-type", "D-type"]`

- `UT_evolution.vector_regs` - `["masks_cfg", "pedestals_cfg", "baselines_cfg", "calib_enables_cfg"]` - registers defined per channel, not per chip


- `UT_evolution.translator` - `dict` mapping channelID for each channel to ECS notation

- `UT_evolution.universal_map` - `dict` mapping ECS for each chip to the `dict` of additional information: 

    - hybrid
    - dcb_device
    - sol40_device
    - sol40_name
    - gbtx_device
    - tell40_device
    - tell40_link
    - pcie40_name
    - flavour
    - source_id
    - maraton_alias
    - maraton_rcm
    - maraton_channel
    - lvr_device
    - lvr_channel
    - caen_crate
    - caen_channel
    - hv_partition
    - sensor_type

- `UT_evolution.UT_map` - `dict` mapping staves and chips to given indices in 2D tabs used for drawing the UT


## Methods
#### at()
`UT_evolution.at(`*index*`)`
    
<ins>Returns</ins>: 

- `dict` with data from a given dump file

<ins>Parameters</ins>:

**index**: *int*
> position of the date corresponding to given dump file on the time axis, starting from zero
#### at_date()
`UT_evolution.at_date(`*date*`)`

<ins>Returns</ins>: 

 - `dict` with data from a given dump file

<ins>Parameters</ins>:

**date**: *str*
> Date corresponding to given dump file. Its format should look like this:
> - `'2024/04/05 18:21:58'` - if the dump file contains data for only one side of the UT
> - `'2024/04/05 18:22:11, 18:21:58'` - if the dump file contains data for both sides of the UT

#### get_data()

`UT_evolution.get_data(`*UT_region*, *register*, *dates* = `None`,        *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False)`

<ins>Returns</ins>: 

- `list` with register values for given *dates*. If the data comes from multiple dates, for non-vector registers it is appended into 1D list of size $N$. For vector registers however, the list is 2D and has the size of $N\times128$.  

<ins>Parameters</ins>:

**UT_region**: `str`
>Region of the UT from which the data comes. It can be one of the following:
> - `"UT"` - for the whole detector 
> - `"UADAQFEE"`/`"UCDAQFEE"` - for only one of the UT sides
> - `"UTaX"`/`"UTaU"`/`"UTbV"`/`"UTbX"` - for one of the detecting planes
> - `"UADAQFEE.UTaX"` etc. - for a given side of the detecting plane
> - `"UTbV_8A"` etc. - for a given stave
> - `"UTbX_8AB_S3"` etc. for a gven sector (4 ASICs)
> - `"UTbX_8AB_S3.Chip0"` etc. for a given chip

**register**: `str`
> name of one of the registers in the dump file

**dates**: `str` or `list` of `str`
> dates corresponding to the given dump files. Each of the dates should have a format like this:
> - `'2024/04/05 18:21:58'` - if the dump file contains data for only one side of the UT
> - `'2024/04/05 18:22:11, 18:21:58'` - if the dump file contains data for both sides of the UT

**filters**: `str`
> additional conditions from `UT_evolution.universal_map` that may be imposed on data. The condition `str` understands two logical operators: `==` and `|`. Example of a filter: `"sensor_type == A | sensor_type == B"`

**start_bit**: `int`
> LSB that should be read from the binary representation of the register's value 

**end_bit**: `int`
> MSB that should be read from the binary representation of the register's value 

**twos_complement**: `bool`
> states if the binary value is signed (`True`) or unsigned (`False`)
    
#### get_data_location()

`UT_evolution.get_data_location(`*UT_region*, *register*, *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False)`

<ins>Returns</ins>: 

- The nested `dict` with dates and ASIC'S ECS as keys, and register values as values

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data)

#### get_mean()

`UT_evolution.get_mean(`*UT_region*, *register*, *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False)`

<ins>Returns</ins>: 

- `numpy.float64` - The mean value of the dataset 

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data)

#### get_min()
 
`UT_evolution.get_min(`*UT_region*, *register*, *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False)`

<ins>Returns</ins>: 

- `numpy.int64` - The minimum value of the dataset

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data)

#### get_max()

`UT_evolution.get_max(`*UT_region*, *register*, *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False)`

<ins>Returns</ins>: 

- `numpy.int64` - The maximum value of the dataset 

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data)

#### get_median()

`UT_evolution.get_median(`*UT_region*, *register*, *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False)`

<ins>Returns</ins>: 

- `numpy.float64` - The median of the dataset 

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data)

#### get_lower_quartile()

`UT_evolution.get_lower_quartile(`*UT_region*, *register*, *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False)`

<ins>Returns</ins>: 

- `numpy.float64` - The lower quartile of the dataset

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data)

#### get_upper_quartile()

`UT_evolution.get_upper_quartile(`*UT_region*, *register*, *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False)`

<ins>Returns</ins>: 

- `numpy.float64` - The upper quartile of the dataset

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data)

#### get_lower_whisker()

`UT_evolution.get_lower_whisker(`*UT_region*, *register*, *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False)`

<ins>Returns</ins>:

- `numpy.int64` - The lower whisker of the dataset

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data)

#### get_upper_whisker()

`UT_evolution.get_upper_whisker(`*UT_region*, *register*, *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False)`

<ins>Returns</ins>:

- `numpy.int64` - The upper whisker of the dataset

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data)

#### get_outliers()

`UT_evolution.get_outliers(`*UT_region*, *register*, *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False)`

<ins>Returns</ins>:

- The nested `dict` with dates and ASIC's ECS as keys, and register values as values. Only those values which are lower than lower whisker and higher than upper whisker are returned. 

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data)

#### get_difference()

`UT_evolution.get_difference(`*UT_region*, *register*, *date1*, *date2*, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False)`

<ins>Returns</ins>:

- `dict` with ASIC's ECS as keys and difference between register values at *date2* and *date1* (val[*date2*] - val[*date1*]). 

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data) except for *date1* and *date2* being used instead of *dates*. 

---
#### plot_hist()

`UT_evolution.plot_hist(`*UT_region*, *register*, *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False`, *fig* = `None`, *ax* = `None`, *\*\*kwargs*`)`

Plots a histogram with all *register* values from given *UT_region* at *dates*.

<ins>Returns</ins>:

- `fig` - `matplotlib.pyplot.figure` object
- `ax` - `matplotlib.axes` object

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data) with three additional ones: 

**fig**: `matplotlib.pyplot.figure`
> matplotlib figure object made before the function call. Useful for plotting multiple hists on the same figure. If set to `None`, the figure object is created inside the function

**ax**: `matplotlib.axes`
> matplotlib axes object made before the function call. Useful for plotting multiple hists on the same figure using subplots. If set to `None`, the axes object is created inside the function

**\*\*kwargs**: `dict`
> dict with additional properties of `matplotlib.pyplot.hist`, like range, bins etc.

#### plot_hist_evolution()

`UT_evolution.plot_hist_evolution(`*UT_region*, *register*, *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False`, *\*\*kwargs*`)`

Plots a histogram with all *register* values from given *UT_region* at *dates*.

<ins>Returns</ins>:

- `fig` - `matplotlib.pyplot.figure` object
- `ax` - `matplotlib.axes` object
- `register_values` - 2D `list` with register values for each date

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data) with one additional one: 

**\*\*kwargs**: `dict`
> dict with additional properties of `matplotlib.pyplot.hist`, like range, bins etc.

#### plot_boxplot()

`UT_evolution.plot_boxplot(`*UT_region*, *register*, *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False`, *fig* = `None`, *ax* = `None`, *\*\*kwargs*`)`

Plots a boxplot with all *register* values from given *UT_region* at *dates*.

<ins>Returns</ins>:

- `fig` - `matplotlib.pyplot.figure` object
- `ax` - `matplotlib.axes` object
- `df` - `pandas.DataFrame` object with all boxplot characteristics

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data) with three additional ones: 

**fig**: `matplotlib.pyplot.figure`
> matplotlib figure object made before the function call. Useful for plotting multiple hists on the same figure. If set to `None`, the figure object is created inside the function

**ax**: `matplotlib.axes`
> matplotlib axes object made before the function call. Useful for plotting multiple hists on the same figure using subplots. If set to `None`, the axes object is created inside the function

**\*\*kwargs**: `dict`
> dict with additional properties of `matplotlib.pyplot.boxplot`

#### plot_boxplot_trend()

Plots a boxplot trend across *dates* for all *register* values from given *UT_region*

`UT_evolution.plot_boxplot_trend(`*UT_region*, *register*, *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False`, *fig* = `None`, *ax* = `None`, *\*\*kwargs*`)`

<ins>Returns</ins>:

- `fig` - `matplotlib.pyplot.figure` object
- `ax` - `matplotlib.axes` object
- `df` - `pandas.DataFrame` object with all boxplot characteristics for all *dates*

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data) with three additional ones: 

**fig**: `matplotlib.pyplot.figure`
> matplotlib figure object made before the function call. Useful for plotting multiple boxplots on the same figure. If set to `None`, the figure object is created inside the function

**ax**: `matplotlib.axes`
> matplotlib axes object made before the function call. Useful for plotting multiple boxplots on the same figure using subplots. If set to `None`, the axes object is created inside the function

**\*\*kwargs**: `dict`
> dict with additional properties of `matplotlib.pyplot.boxplot`

#### plot_trend()

`UT_evolution.plot_trend(`*ECS*, *register* *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False`, *\*\*kwargs*`)`

Plots a *register* value trend across *dates* for given *ECS*

<ins>Returns</ins>:

- `fig` - `matplotlib.pyplot.figure` object
- `ax` - `matplotlib.axes` object

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data) except for *UT_region*. There are two additional ones: 

**ECS**: `str`
> used in the same manner as UT_region. For non-vector registers it has to be specified for a given ASIC, e.g.: `"UTaX_1CT_M1E.Chip0"`, and for vector registers it has to be specified for a given channel, e.g: `"UTaX_1CT_M1E.Chip0.Ch20"`

**\*\*kwargs**: `dict`
> dict with additional properties of `matplotlib.pyplot.plot`

#### plot_waterfall()

`UT_evolution.plot_waterfall(`*UT_region*, *register*, *dates* = `None`, *filters* = `""`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False`, *fig* = `None`, *ax* = `None`, *\*\*kwargs*`)`

plots waterfall plot with *register* values for a given stave (for non-vector registers) or a given chip (for vector registers)

<ins>Returns</ins>:

- `fig` - `matplotlib.pyplot.figure` object
- `ax` - `matplotlib.axes` object
- `register_values` - 2D `list` with register values for each date

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data) except for additional constraints for *UT_region* and three additional ones:

**UT_region**: `str`
> Region of the UT from which the data comes. It can be only one of the following:
> - `"UTbV_8A"` etc. - for a given stave (only for non-vector registers)
> - `"UTbX_8AB_S3.Chip0"` etc. for a given chip (only for vector-registers)

**fig**: `matplotlib.pyplot.figure`
> matplotlib figure object made before the function call. Useful for plotting multiple waterfall plots on the same figure. If set to `None`, the figure object is created inside the function

**ax**: `matplotlib.axes`
> matplotlib axes object made before the function call. Useful for plotting multiple waterfall plots on the same figure using subplots. If set to `None`, the axes object is created inside the function

**\*\*kwargs**: `dict`
> dict with additional properties of `matplotlib.pyplot.imshow`


#### plot_UT_plane()

`UT_evolution.plot_UT_plane(`*UT_plane*, *register*, *dates* = `None`, *option* = `None`, *fs* = `(15,10)`, *start_bit* = `None`, *end_bit* = `None`, *twos_complement*=`False`, *side* = `None`, *mirroring* = `True`, *\*\*kwargs*`)`

plots 2D maps of *UT_plane* (or its half) with *register* values for each date in *dates*. For non-vector registers only the values are plotted. For vector registers multiple option are available, like mean, median etc. Every 2D map of the UT follows simplified geometrical position scheme (ASICS always from 0 to 3)

<ins>Returns</ins>:

- `UT_tabs` - `list` of 2D `numpy.ndarray`. Every 2D array corresponds to the plotted UT map. 

<ins>Parameters</ins>:

Exactly the same set of parameters as [here](#get_data) except for the fact that there is no *filters* parameter, *UT_plane* is used instead of *UT_region* and there are 6 additional parameters:

**UT_plane**: `str`
> Detection plane of the UT to be plotted: `"UTaX"`/`"UTaU"`/`"UTbV"`/`"UTbX"` or `None` (used for plotting all 4 layers at once for particular date)

**option**: `str`
> For non-vector registers only two options are available:
> - `None` - represents just plotting the values of the registers
> - `"differences"` - for the first date, just the values are plotted. For the next dates, it's the number of non-zero differences in register's values

> For vector registers there are multiple options:
> - `"mean"`
> - `"min"`
> -  `"max"`
> - `"median"`
> - `"upper_quartile"`
> - `"lower_quartile"`
> - `"upper_whisker"` 
> - `"lower_whisker"`
> - `"outliers"` - plots the number of outliers wrt. to the whole detector
> - `"outliers_plane"` - plots the number of outliers wrt. to the drawn detecting plane
> - `"outliers_sensor_type"` - plots the number of outliers wrt. to the sensor type
> - `"outliers_plane_sensor_type"` - plots the number of outliers wrt. to the sensor type and the drawn detecting plane
> - `"outliers_chip"` - plots the number of outliers wrt. to the chip
> - `"differences"`- for the first date, just the mean values are plotted. For the next dates, it's the number of non-zero differences in register's values

**fs**: `tuple`
> figure size of a single UT plane plot

**side**: `str`
> side of the UT to be plotted: `"A"`/`"C"`. If set to `None` whole detecting plane is plotted. 

**mirroring**: `bool`
> the way of plotting the chips. If set to True, sectors W and E are displayed as in the detector layout. If set to False, W is always on the left and E is always on the right of the module. Chips are numbered from left to right in both cases. 

**\*\*kwargs**: `dict`
> dict with additional properties of `matplotlib.pyplot.imshow`




