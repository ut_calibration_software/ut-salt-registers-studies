# UT SALT Registers Studies

This is a UT-calibration-software tool that helps you to analyse time evolution of the various SALT registers. 

The following functionalities are now implemented:

- Drawing histograms of a given register values for each timestamp (per side, plane, stave, module etc.)
- Drawing waterfall plots
- Drawing 2D UT-maps


# Reading values
Values for a given timestamp are written into a nested dictionary of the following structure:

```python
{'UADAQFEE':
    {'UTaX_1AT_M1E':
        {'Chip0':
            {
                <register name>: <register value>,
                ...
            }
         'Chip1':
         ...
        },
     'UTaX_1AT_M1W':
     ...
    },
 'UCDAQFEE':  
 ...
 }
 ```